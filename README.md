# generator-wpstack

Yeoman generator for creating and scaffolding wordpress projects.

This generator lets you easily create wordpress projects that include:

**Wordpress**

Latest wordpress installation.

https://wordpress.org/

**Corcel**

Corcel is a class collection created to retrieve WordPress database data using a better syntax. It uses the Eloquent ORM developed for the Laravel Framework.

https://github.com/jgrossi/corce

**Wordpress API v2**

Access your WordPress site's data through an easy-to-use HTTP REST API.

http://v2.wp-api.org/

**Timber (Pug & Twig)**

Timber: a faster, easier and more powerful way to build themes with Pug(former Jade) & Twig templates.

http://upstatement.com/timber/

**Babel (ECMAScript 6)**

The compiler for writing next generation JavaScript

https://babeljs.io/

**Browserify**

Browserify lets you require('modules') in the browser by bundling up all of your dependencies.

http://browserify.org/

**CMB2**

CMB2 is a developer's toolkit for building metaboxes, custom fields, and forms for WordPress that will blow your mind.

https://github.com/WebDevStudios/CMB2

**Gulp**

Automate and enhance your workflow, with gulp the streaming build system.

http://gulpjs.com/
